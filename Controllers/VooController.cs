﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AeroAPI.DTO;
using AeroAPI.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AeroAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VooController : ControllerBase
    {
        private readonly AeroContext _context;

        public VooController(AeroContext context)
        {
            _context = context;
        }

        [HttpGet()]
        public IActionResult GetComFiltro([FromQuery]FiltroVooDTO filtro)
        {
            var listaParaRetornar =
                _context.Voos.Where(
                    item =>
                    item.LocalDestinoId == filtro.DestinoId
                    && item.LocalOrigemId == filtro.OrigemId
                    && item.DataIda > filtro.DataInicial
                    && item.DataIda <= filtro.DataFinal);
            return Ok(Convert(listaParaRetornar.ToList()));
        }

        private IEnumerable<dynamic> Convert(List<Voo> lista)
        {
            return lista.Select(item => new
            {
                item.Id,
                item.DataIda,
                item.DataVolta,
                item.LocalDestinoId,
                item.LocalOrigemId,
                item.NumeroParadas,
                item.Preco,
                PrecoComDesconto = item.Preco * 0.9
            });
        }
    }
}
