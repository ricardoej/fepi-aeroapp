﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AeroAPI.Migrations
{
    public partial class AddLotacaoMaximaToVoo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LotacaoMaxima",
                table: "Voos",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LotacaoMaxima",
                table: "Voos");
        }
    }
}
