INSERT INTO public."Locais" ("Id", "Nome", "Cidade", "Estado", "Sigla") VALUES
(1, 'Aeroporto Internacional de São Paulo', 'Guarulhos', 'São Paulo', 'GRU'),
(2, 'Aeroporto de São Paulo/Congonhas', 'São Paulo', 'São Paulo', 'CGH'),
(3, 'Aeroporto Santos Dumont', 'Rio de Janeiro', 'Rio de Janeiro', 'SDU'),
(4, 'Aeroporto Internacional de Curitiba', 'Curitiba', 'Paraná', 'CWB'),
(5, 'Aeroporto Internacional Juscelino Kubitschek', 'Brasília', 'Distrito Federal', 'BSB'),
(6, 'Aeroporto Internacional de Belo Horizonte-Confins', 'Belo Horizonte', 'Minas Gerais', 'CFN');


INSERT INTO public."Voos"("Id", "DataIda", "DataVolta", "LocalOrigemId", "LocalDestinoId", "NumeroParadas", "TempoIda", "TempoVolta", "Preco", "LotacaoMaxima") VALUES
(1, '2020-11-10 20:50:00', '2020-11-11 20:50:00', 1, 3, 0, '1:00:00', '1:00:00', 250.90, 150),
(2, '2020-11-11 20:50:00', '2020-11-12 20:50:00', 2, 3, 0, '1:00:00', '1:00:00', 250.90, 150),
(3, '2020-11-12 20:50:00', '2020-11-13 20:50:00', 1, 5, 0, '1:00:00', '1:00:00', 250.90, 150),
(4, '2020-11-13 20:50:00', '2020-11-14 20:50:00', 1, 6, 0, '1:00:00', '1:00:00', 250.90, 150),
(5, '2020-11-14 20:50:00', '2020-11-15 20:50:00', 2, 5, 0, '1:00:00', '1:00:00', 250.90, 150),
(6, '2020-11-15 20:50:00', '2020-11-16 20:50:00', 2, 6, 0, '1:00:00', '1:00:00', 250.90, 150),
(7, '2020-11-16 20:50:00', '2020-11-17 20:50:00', 2, 6, 0, '1:00:00', '1:00:00', 250.90, 150);